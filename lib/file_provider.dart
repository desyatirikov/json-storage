import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:path_provider/path_provider.dart';

enum ContentType {
  text,
  json,
  bytes,
}

class FileProvider {
  final String fileName;
  final Duration delay;
  File _file;

  Timer _delayTimer;

  FileProvider(
    this.fileName, {
    Duration delay,
  }) : this.delay = delay ?? Duration(seconds: 5);

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    if (_file == null) {
      final path = await _localPath;
      _file = File("$path/$fileName");
      if (!(await _file.exists())) {
        await _file.create(recursive: true);
      }
    }
    return _file;
  }

  Future<String> readString() async {
    String content;
    try {
      final file = await _localFile;
      content = await file.readAsString();
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
    }
    return content;
  }

  Future<Map<String, dynamic>> readJson() async {
    String contents = await readString();
    Map<String, dynamic> json;
    if (contents != null && contents.isNotEmpty) {
      try {
        json = jsonDecode(contents);
      } catch (exception, stackTrace) {
        print(exception);
        print(stackTrace);
      }
    }
    
    json ??= {};

    return json;
  }

  Future<dynamic> readBytes() async {
    Uint8List bytes;
    try {
      final file = await _localFile;

      // Read the file
      bytes = await file.readAsBytes();
    } catch (exception, stackTrace) {
      print(exception);
      print(stackTrace);
    }
    return bytes;
  }

  Future<File> writeString(String content) async {
    final file = await _localFile;
    return file.writeAsString(content, flush: true);
  }

  Future<File> writeBytes(Uint8List content) async {
    final file = await _localFile;
    return file.writeAsBytes(content, flush: true);
  }

  Future<File> writeJson(dynamic content) async {
    final file = await _localFile;
    return file.writeAsString(jsonEncode(content), flush: true);
  }

  Future<File> write(dynamic content, {ContentType contentType}) async {
    final file = await _localFile;

    switch (contentType) {
      case ContentType.text:
        return writeString(content);
      case ContentType.json:
        return writeJson(content);
      case ContentType.bytes:
        return writeBytes(content);
      default:
        return file.writeAsString(content.toString(), flush: true);
    }
  }

  void writeDelayed(dynamic content, {ContentType contentType}) {
    if (_delayTimer != null) {
      _delayTimer.cancel();
      _delayTimer = null;
    }

    _delayTimer = new Timer(delay, () {
      write(content, contentType: contentType);
      _delayTimer.cancel();
      _delayTimer = null;
    });
  }
}
